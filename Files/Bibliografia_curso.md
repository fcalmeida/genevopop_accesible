# Bibliografía Básica

Estas referencias no pretenden ser una lista exhaustiva de cada tema. 
Fundamentalmente se limitan a las ~10-20 principales. 
Los emojis ofrecen pistas (obviamente subjetivas) de su relevancia.

* :exclamation: -> Fundamental  
* :v: -> Icónica  
* :bomb: -> Revolucionaria  
* :+1: -> Interesante 
* :heart: -> Lovely 
* :rocket: -> Tendencia  

Si no puede acceder de forma gratuita a través del link, utilice [`Sci-Hub`](https://sci-hub.se/).

_____

## Módulo 1: Genómica Humana, Comparativa y Funcional 

### Tema 1. Genomas Humanos y Tecnología de Secuenciación 

* Marra, M., et al. (2001). [A physical map of the human genome](https://www.nature.com/articles/35057157) :exclamation: 
* Lander, E. S.,  et al. (2001). [Initial sequencing and analysis of the human genome](http://doi.org/10.1038/35057062) :v:
* Venter, J. C., et al. (2001). [The sequence of the human genome](http://doi.org/10.1126/science.1058040) :v: 
* Waterston, R. H., et al. (2002). [On the sequencing of the human genome](http://doi.org/10.1073/pnas.042692499) :+1:
* Collins, F. S. (2003). [The Human Genome Project: Lessons from Large-Scale Biology](http://doi.org/10.1126/science.1084564). :+1:
* Levy, S., et al. (2007). [The diploid genome sequence of an individual human](http://doi.org/10.1371/journal.pbio.0050254) :v: 
* Wheeler, D. A., et al. (2008). [The complete genome of an individual by massively parallel DNA sequencing](http://doi.org/10.1038/nature06884) :bomb:	 
* Bentley, D. R.,  et al. (2008). [Accurate whole human genome sequencing using reversible terminator chemistry](http://doi.org/10.1038/nature07517) :bomb:	 
* Stephens, Z. D., et al. (2015). [Big Data: Astronomical or Genomical?](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.1002195) :+1:
* Goodwin, S., et al. (2016). [Coming of age: ten years of next-generation sequencing technologies](http://doi.org/10.1038/nrg.2016.49) :heart:
* van Dijk, E. L., et al (2018). [The Third Revolution in Sequencing Technology](http://doi.org/10.1016/j.tig.2018.05.008) :rocket:
* Rachel M. Sherman, et al. (2018). [Assembly of a pan-genome from deep sequencing of 910 humans of African descent](https://www.nature.com/articles/s41588-018-0273-y) :+1:

### Tema 2. Genómica Funcional 

* Cheng, J., et. al.  (2005). [Transcriptional Maps of 10 Human Chromosomes at 5-Nucleotide Resolution](http://science.sciencemag.org/content/308/5725/1149) :exclamation:
* The ENCODE Project Consortium. (2004). [The ENCODE (ENCyclopedia Of DNA Elements Project](http://doi.org/10.1126/science.1105136) :v:
* Khaitovich, P., et al. (2006). [Evolution of primate gene expression](http://doi.org/10.1038/nrg1940) :bomb:
* Gingeras, T. R. (2007). [Origin of phenotypes: genes and transcripts](http://doi.org/10.1101/gr.6525007) :exclamation:
* ENCODE Project Consortium, et al. (2007). [Identification and analysis of functional elements in 1% of the human genome by the ENCODE pilot project](http://doi.org/10.1038/nature05874) :v:
* Gerstein, M. B., et al. (2007). [What is a gene, post-ENCODE? History and updated definition](http://doi.org/10.1101/gr.6339607) :+1:
* Wang, Z., et al. (2009). [RNA-Seq: a revolutionary tool for transcriptomics](http://doi.org/10.1038/nrg2484) :rocket:
* ENCODE Project Consortium, et al. (2012). [An integrated encyclopedia of DNA elements in the human genome](http://doi.org/10.1038/nature11247) :v:
* Doolittle, W. F. (2013). [Is junk DNA bunk? A critique of ENCODE](https://www.pnas.org/content/110/14/5294) :+1:
* Graur, D., et al. (2013). [On the immortality of television sets:“function” in the human genome according to the evolution-free gospel of ENCODE](https://academic.oup.com/gbe/article/5/3/578/583411) :+1:
* Shalem, O., et al. (2015). [High-throughput functional genomics using CRISPR-Cas9](http://doi.org/10.1038/nrg3899) :rocket:
* Rowley J & V. Corces (2018). [Organizational principles of 3D genome architecture](https://www.nature.com/articles/s41576-018-0060-8) :heart:
* Reimand, J. et al. (2019).[Pathway enrichment analysis and visualization of omics data using g:Profiler, GSEA, Cytoscape and EnrichmentMap.](https://www.nature.com/articles/s41596-018-0103-9):heart:

### Tema 3. Genómica Comparativa 

* Force, A., et al.(1999) [Preservation of duplicate genes by complementary, degenerative mutations](https://www.ncbi.nlm.nih.gov/pubmed/10101175) :+1:
* Lynch, M., et al. (2001). [The probability of preservation of a newly arisen gene duplicate](https://www.ncbi.nlm.nih.gov/pubmed/10101175) :v:
* Lynch, M., & Conery, J. S. (2003). [The origins of genome complexity](http://doi.org/10.1126/science.1089370) :v:
* Bejerano, G., et al.(2004). [Ultraconserved elements in the human genome](http://doi.org/10.1126/science.1098119) :exclamation:
* Pal, C. (2006) [An integrated view of protein evolution](http://www.nature.com/nrg/journal/vaop/ncurrent/full/nrg1838.html) :heart:
* Lynch, M. (2007). [The frailty of adaptive hypotheses for the origins of organismal complexity](http://doi.org/10.1073/pnas.0702207104) :+1:
* Lynch M. (2007). [The Origins of Genome Architecture](https://www.amazon.com/Origins-Genome-Architecture-Michael-Lynch/dp/0878934847/ref=sr_1_1?s=books&ie=UTF8&qid=1532903415&sr=1-1&keywords=the+origins+of+genome+architecture#customerReviews). 1st edition. :bomb:
* Innan, H., & Kondrashov, F. (2010). [The evolution of gene duplications: classifying and distinguishing between models](http://doi.org/10.1038/nrg2689) :heart:
* Colbourne, J.K., et al. (2011).[The ecoresponsive genome of Daphnia pulex](http://doi.org/10.1126/science.1197761) :+1:
* Serra, F. et al. (2012) [Neutral Theory Predicts the Relative Abundance and Diversity of Genetic Elements in a Broad Array of Eukaryotic Genomes](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0063915) :+1:
* Ellegren H. & N. Galtier (2016). [Determinants of genetic diversity](https://www.nature.com/articles/nrg.2016.58) :heart:
_____

## Módulo 2: Variabilidad Genómica y Estructura Poblacional 

### Tema 4. Variabilidad Genómica Poblacional 

* Tajima, F. et al (1998). [The amount and pattern of DNA polymorphism under the neutral mutation hypothesis.](https://www.ncbi.nlm.nih.gov/pubmed/9766959) :exclamation:
* Wang, D. et al. (1998). [Large-scale identification, mapping, and genotyping of single-nucleotide polymorphisms in the human genome.](https://www.ncbi.nlm.nih.gov/pubmed/9582121) :+1:
* Cargill, M. et al. (1999) [Characterization of single-nucleotide polymorphisms in coding regions of human genes.](https://www.ncbi.nlm.nih.gov/pubmed/10391209) :+1:
* Przeworski M, et al. (1999). [Adjusting the focus on human variation](https://www.cell.com/trends/genetics/fulltext/S0168-9525(00)02030-8?code=cell-site&mobileUi=0) :exclamation:
* Kruglyak, L & D. Nickerson (2001). [Variation is the spice of life](https://www.nature.com/articles/ng0301_234) :v:
* The International HapMap Consortium (2003). [The International HapMap Project](https://www.nature.com/articles/nature02168) :exclamation:
* The International HapMap Consortium (2005). [A haplotype map of the human genome](https://www.nature.com/articles/nature04226) :v:
* Sherer S. et al (2007). [Challenges and standards in integrating surveys of structural variation.](https://www.ncbi.nlm.nih.gov/pubmed/17597783) :v:
* International HapMap Consortium (2007). [A second generation human haplotype map of over 3.1 million SNPs](https://www.ncbi.nlm.nih.gov/pubmed/17943122) :exclamation:
* International HapMap 3 Consortium, et al. (2010). [Integrating common and rare genetic variation in diverse human populations.](https://www.ncbi.nlm.nih.gov/pubmed/20811451) :exclamation:
* The 1,000 Genomes Project Consortium (2010). [A map of human genome variation from population-scale sequencing](https://www.nature.com/articles/nature09534) :v:
* Alkan C., et al. (2011). [Genome structural variation discovery and genotyping](https://www.ncbi.nlm.nih.gov/pubmed/21358748) :heart:
* Mills R. et al (2011). [Mapping copy number variation by population-scale genome sequencing.](https://www.ncbi.nlm.nih.gov/pubmed/21293372) :exclamation:
* The 1,000 Genomes Project Consortium (2012). [An integrated map of genetic variation from 1,092 human genomes](https://www.nature.com/articles/nature11632) :exclamation:
* Weischenfeldt J., et al. (2013). [Phenotypic impact of genomic structural variation: insights from and for human disease.](https://www.ncbi.nlm.nih.gov/pubmed/23329113) :exclamation:
* The 1000 Genomes Project Consortium (2015). [A global reference for human genetic variation](https://www.nature.com/articles/nature15393) :heart:
* Sudmant P. et al (2015). [An integrated map of structural variation in 2,504 human genomes](https://www.ncbi.nlm.nih.gov/pubmed/21293372) :v:
* Sudmant P. et al (2015). [Global diversity, population stratification, and selection of human copy-number variation](https://www.ncbi.nlm.nih.gov/pubmed/26249230) :heart:

### Tema 5. Demografía y Estructura Poblacional 
* Rosenberg, N. & M. Nordborg.(2002). [Genealogical trees, coalescent theory and the analysis of genetic polymorphisms](https://www.nature.com/articles/nrg795):exclamation:
* Novembre J., et al. (2008). [Genes mirror geography within Europe](https://www.nature.com/articles/nature07331).:v:
* Holsinger K. & B.S. Weir. (2009) [Genetics in geographically structured populations: defining, estimating and interpreting F(ST).](https://www.nature.com/articles/nrg2611). :heart:
* O´Dushlaine, C., et al. (2010). [Genes predict village of origin in rural Europe](https://www.ncbi.nlm.nih.gov/pubmed/20571506). :+1:
* Coventry, A. et al. (2010) [Deep resequencing reveals excess rare recent variants consistent with explosive population growth](https://www.ncbi.nlm.nih.gov/pubmed/21119644). :bomb:
* Novembre J. & S. Ramachandran S (2011). [Perspectives on human population structure at the cusp of the sequencing era.](https://www.ncbi.nlm.nih.gov/pubmed/21801023) :heart:
* Daniel Wegmann, D. Et al. (2011) [Recombination rates in admixed individuals identified by ancestry-based inference](https://www.nature.com/articles/ng.894). :rocket:
* Leutenegger, A.L. et al.(2011) [Consanguinity around the world: what do the genomic data of the HGDP-CEPH diversity panel tell us?](https://www.nature.com/articles/ejhg2010205):+1:
* Keinan, A. & A. Clark (2012). [Recent explosive human population growth has resulted in an excess of rare genetic variants](https://www.ncbi.nlm.nih.gov/pubmed/22582263). :v:
* Nelson, M.  (2012). [An abundance of rare functional variants in 202 drug target genes sequenced in 14,002 people](). :+1:
* Schraiber JG & JM Akey. (2015). [Methods and models for unravelling human evolutionary history.](https://www.ncbi.nlm.nih.gov/pubmed/26553329) :heart:
* Peter, B.M. (2016). [Admixture, Population Structure, and F-Statistics.](https://www.ncbi.nlm.nih.gov/pubmed/26857625) :rocket:
* Ceballos, F. (2018). [Runs of homozygosity: windows into population history and trait architecture.](https://www.ncbi.nlm.nih.gov/pubmed/29335644) :+1:
_____

## Módulo 3: Poblamiento y Selección Natural 

### Tema 6:  Poblamiento Continental y Genomas Ancestrales  

* Li, J. Z., et al. (2008). [Worldwide Human Relationships Inferred from Genome-Wide Patterns of Variation](https://www.ncbi.nlm.nih.gov/pubmed/18292342) :v:
* Green, R. E., et al. (2010). [A draft sequence of the Neandertal genome](http://science.sciencemag.org/content/328/5979/710) :v:
* Reich, D., et al. (2010). [Genetic history of an archaic hominin group from Denisova Cave in Siberia](https://www.nature.com/articles/nature09710).:exclamation:
* Reich, D., et al. (2010). [Reconstructing Native American population history](https://www.nature.com/articles/nature11258). :v:
* Meyer, M., et al. (2012). [A high-coverage genome sequence from an archaic Denisovan individual](https://www.ncbi.nlm.nih.gov/pubmed/22936568). :bomb:
* Henn, B., Cavalli-Sforza, L., & M. Feldman. (2012). [The great human expansion](https://www.ncbi.nlm.nih.gov/pubmed/23077256) :exclamation:
* Prüfer, K., et al. (2014). [The complete genome sequence of a Neanderthal from the Altai Mountains](https://www.ncbi.nlm.nih.gov/pubmed/22936568). :exclamation:
* Sankararaman, S. et al. (2014). [The genomic landscape of Neanderthal ancestry in present-day humans](https://www.nature.com/articles/nature12961). :exclamation:
* Moreno Estrada A., et al. (2014). [Human genetics. The genetics of Mexico recapitulates Native American substructure and affects biomedical traits.](https://www.ncbi.nlm.nih.gov/pubmed/24926019)
* Vernot B. & J. Akey (2014). [Resurrecting surviving Neandertal lineages from modern human genomes.](https://www.ncbi.nlm.nih.gov/pubmed/24476670) :+1:
* Racimo F, et al. (2015). [Evidence for archaic adaptive introgression in humans](https://www.ncbi.nlm.nih.gov/pubmed/25963373). :heart:
* Homburger, J. et al. (2015). [Genomic Insights into the Ancestry and Demographic History of South America](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1005602). :v:
* Nielsen, R. et al.(2017). [Tracing the peopling of the world through genomics](https://www.nature.com/articles/nature21347) :heart:
* Posth C., et al. (2018). [Reconstructing the Deep Population History of Central and South America](https://www.sciencedirect.com/science/article/pii/S0092867418313801) :bomb:
* de la Fuente C., et al (2018). [Genomic insights into the origin and diversification of late maritime hunter-gatherers from the Chilean Patagonia](https://www.ncbi.nlm.nih.gov/pubmed/29632188). :exclamation:
* Moreno-Mayar JV. et al (2018). [Early human dispersals within the Americas](https://www.ncbi.nlm.nih.gov/pubmed/30409807) :exclamation:

### Tema 7:  Pruebas de Neutralidad  y Selección Natural en Genomas Completos

* Nielsen, R. (2001). [Statistical tests of selective neutrality in the age of genomics.](https://www.nature.com/articles/6888950) :rocket:
* Sabeti, P. C., (2002). [Detecting recent positive selection in the human genome from haplotype structure.](https://www.nature.com/articles/nature01140) :exclamation: 
* Bamshad, M., & Wooding, S. P. (2003). [Signatures of natural selection in the human genome.](https://www.nature.com/articles/nrg999) :heart: 
* Bustamante, C. D., et al. (2005). [Natural selection on protein-coding genes in the human genome.](https://www.nature.com/articles/nature04240) :v:
* Sabeti, P. C., et al. (2006). [Positive natural selection in the human lineage.](https://www.ncbi.nlm.nih.gov/pubmed/16778047) :v:
* Voight, B. F., et al. (2006). [A map of recent positive selection in the human genome.](https://journals.plos.org/plosbiology/article?id=10.1371/journal.pbio.0040072) :v:
* Nielsen, R., (2007). [Recent and ongoing selection in the human genome.](https://www.ncbi.nlm.nih.gov/pubmed/17943193) :heart:
* Hancock, A. M., et al. (2010). [Human adaptations to diet, subsistence, and ecoregion are due to subtle shifts in allele frequency.](https://www.pnas.org/content/107/Supplement_2/8924) :rocket:
* Simonson, T. S., et al. (2010). [Genetic evidence for high-altitude adaptation in Tibet.](https://www.ncbi.nlm.nih.gov/pubmed/20466884) :+1:
* Laland, K. N., et al. (2010) [How culture shaped the human genome: bringing genetics and the human sciences together.](https://www.nature.com/articles/nrg2734) :heart:
* Xu, S., et al. (2011). [A genome-wide search for signals of high-altitude adaptation in tibetans.](https://www.ncbi.nlm.nih.gov/pubmed/20961960) :+1: 
* Vitti, J. J., et al. (2012). [Human evolutionary genomics: ethical and interpretive issues.](https://www.ncbi.nlm.nih.gov/pubmed/22265990) :+1:
* Alkorta-Aranburu, G. et al. (2012). [The Genetic Architecture of Adaptations to High Altitude in Ethiopia](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1005004) :+1:
* Vitti, J. J., et al. (2013). [Detecting Natural Selection in Genomic Data.](https://www.ncbi.nlm.nih.gov/pubmed/24274750) :heart:
* Scheinfeldt, L.  & S. Tishkoff (2013). [Recent human adaptation: genomic approaches, interpretation and insights.](https://www.ncbi.nlm.nih.gov/pubmed/24052086) :heart: 
* Berg, J. et al. (2014). [A Population Genetic Signal of Polygenic Adaptation.](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1004412) :+1: 
* Sheehan, M. J. & M. Nachman (2014) [Morphological and population genomic evidence that human faces have evolved to signal individual identity.](https://www.nature.com/articles/ncomms5800) :exclamation: 
* Karlsson, E. et al (2014). [Natural selection and infectious disease in human populations.](https://www.nature.com/articles/nrg3734) :heart:
* Huerta-Sanchez, E., et al. (2014). [Altitude adaptation in Tibetans caused by introgression of Denisovan-like DNA.](https://www.ncbi.nlm.nih.gov/pubmed/25043035) :exclamation: 
* Garup, N. et al. (2015). [Recent Selective Sweeps in North American Drosophila melanogaster Show Signatures of Soft Sweeps](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1005004) :exclamation: 
* Fan, S., (2016). [Going global by adapting local: A review of recent human adaptation.](http://science.sciencemag.org/content/354/6308/54) :heart: 
* Lindo, J. et al. (2018) [The genetic prehistory of the Andean highlands 7000 years BP though European contact](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6224175/). :+1:

_____
## Módulo 4: Genómica de Enfermedades y Medicina de Precisión 

### Tema 8:  Genómica de Enfermedades             

 * Reich, D. & E.Lander. (2001). [On the allelic spectrum of human disease.](https://www.ncbi.nlm.nih.gov/pubmed/11525833). :v:
 * TWTCCC (2007).[Genome-wide association study of 14,000 cases of seven common diseases and 3,000 shared controls](https://www.nature.com/articles/nature05911) :v:
 * Mccarthy, M. I. et al. (2008)[Genome-wide association studies for complex traits: consensus, uncertainty and challenges.](https://www.ncbi.nlm.nih.gov/pubmed/18398418) :heart:
 * Manolio, T.,  et al. (2009). [Finding the missing heritability of complex diseases.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2831613/) :exclamation:
 * Singleton, A. B., et al. (2010) [Towards a complete resolution of the genetic architecture of disease.](https://www.ncbi.nlm.nih.gov/pubmed/20813421) :exclamation:
 * Manolio, T. A. (2010). [Genomewide association studies and assessment of the risk of disease.](https://www.nejm.org/doi/full/10.1056/NEJMra0905980) :heart:
 * Yang, J. et al. (2010) [Common SNPs explain a large proportion of the heritability for human height.](https://www.ncbi.nlm.nih.gov/pubmed/20562875) :+1:
 * Ng, S. B. et al. (2010) [Exome sequencing identifies the cause of a mendelian disorder.](https://www.ncbi.nlm.nih.gov/pubmed/19915526) :rocket:
 * Bamshad, M. J., et al. (2011). [Exome sequencing as a tool for Mendelian disease gene discovery.](https://www.ncbi.nlm.nih.gov/pubmed/21946919) :heart:
 * Barabási, A., et al. (2011) [Network medicine: a network-based approach to human disease.](https://www.nature.com/articles/nrg2918) :+1:
 * Gibson, G. (2012). [Rare and common variants: twenty arguments.](https://www.ncbi.nlm.nih.gov/pubmed/22251874) :exclamation:
 * Zuk, O., et al. (2012). [The mystery of missing heritability: Genetic interactions create phantom heritability.](https://www.ncbi.nlm.nih.gov/pubmed/22223662) :+1:
 * Huang, W, et al. (2012) [Epistasis dominates the genetic architecture of Drosophila quantitative traits](https://www.ncbi.nlm.nih.gov/pubmed/22949659) :bomb:
 * Yang, Y., et al. (2013). [Clinical Whole-Exome Sequencing for the Diagnosis of Mendelian Disorders.](http://doi.org/10.1056/NEJMoa1306555) :+1:
 * Marigota, U. & A. Navarro (2013). [High Trans-ethnic Replicability of GWAS Results Implies Common Causal Variants](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1003566) :exclamation:
 * Garcia-Alonso, L. et al. (2014) [The role of the interactome in the maintenance of deleterious variability in human populations.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4299661/):exclamation:
 * Lek, M., et al. (2016). [Analysis of protein-coding genetic variation in 60,706 humans.](https://www.ncbi.nlm.nih.gov/pubmed/27535533) :bomb:
 * Visscher, P. M., et al (2017). [10 Years of GWAS Discovery: Biology, Function, and Translation](https://www.ncbi.nlm.nih.gov/pubmed/28686856) :heart:
 * Huttlin, E. L., et al. (2017). [Architecture of the human interactome defines protein communities and disease networks.](https://www.ncbi.nlm.nih.gov/pubmed/28514442) :exclamation:
 * Rodríguez, J. A., et al. (2017). [Antagonistic pleiotropy and mutation accumulation influence human senescence and disease.](https://www.nature.com/articles/s41559-016-0055)  :heart:
 * Boyle, E. et al. (2017) [An Expanded View of Complex Traits: From Polygenic to Omnigenic](https://www.ncbi.nlm.nih.gov/pubmed/28622505) :bomb:

### Tema 9:  Medicina de Precisión I

 * Ng, P., et al. (2009). [An agenda for personalized medicine.](https://www.ncbi.nlm.nih.gov/pubmed/19812653) :v:
 * Handyside, A. H. et al. (2010). [Karyomapping: a universal method for genome wide analysis of genetic disease based on mapping crossovers between parental haplotypes.](https://www.ncbi.nlm.nih.gov/pubmed/19858130) :rocket:
 * Lo, Y. M., et al. (2010). [Maternal plasma DNA sequencing reveals the genome-wide genetic and mutational profile of the fetus.](https://www.ncbi.nlm.nih.gov/pubmed/21148127) :bomb:
 * Bloss, C. S., et al., (2011). [Direct-to-consumer personalized genomic testing.](https://www.ncbi.nlm.nih.gov/pubmed/21828075).:+1:
 * Kingsmore, S. (2012). [Comprehensive carrier screening and molecular diagnostic testing for recessive childhood diseases.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3392137/) :exclamation:
 * Lazarin, G. A. et al. (2012). [An empirical estimate of carrier frequencies for 400+ causal Mendelian variants: results from an ethnically diverse clinical sample of 23,453 individuals.](https://www.ncbi.nlm.nih.gov/pubmed/22975760) :+1:
 * Lo, Y. M. & R. W. Chiu. (2012). [Genomic analysis of fetal nucleic acids in maternal blood.](https://www.ncbi.nlm.nih.gov/pubmed/22657389) :v:
 * Ahn, J. W., et al. (2013). [Array CGH as a first line diagnostic test in place of karyotyping for postnatal referrals - results from four years' clinical application for over 8,700 patients.](https://europepmc.org/abstract/pmc/pmc3632487) :v:
 * Treff N. et al. (2013). [Evaluation of targeted next-generation sequencing–based preimplantation genetic diagnosis of monogenic disease.](https://www.ncbi.nlm.nih.gov/pubmed/23312231) :rocket:
 * Fiorentino, F.et al. (2014). [Application of next-generation sequencing technology for comprehensive aneuploidy screening of blastocysts in clinical preimplantation genetic screening cycles.](https://www.ncbi.nlm.nih.gov/pubmed/25336713) :v:
 * Natesan, S. A. et al. (2014) [Genome-wide karyomapping accurately identifies the inheritance of single-gene defects in human preimplantation embryos in vitro.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4225458/) :exclamation:
 * Easton, D. F.,  et al. (2015). [Gene-Panel Sequencing and the Prediction of Breast-Cancer Risk.](https://www.nejm.org/doi/full/10.1056/nejmsr1501341) :+1:
 * Läll K,et al. (2017). [Personalized risk prediction for type 2 diabetes: The potential of genetic risk scores.](https://www.ncbi.nlm.nih.gov/pubmed/27513194) :exclamation:
 * Khera, A. et al (2018). [Genome-wide polygenic scores for common diseases identify individuals with risk equivalent to monogenic mutations.](https://www.ncbi.nlm.nih.gov/pubmed/30104762) :rocket:
 * Bycroft C., et al. (2018) [UK Biobank resource with deep phenotyping and genomic data](https://www.nature.com/articles/s41586-018-0579-z) :v:
 * Adams, D. et al, (2018). [Next-Generation Sequencing to Diagnose Suspected Genetic Disorders](https://www.nejm.org/doi/full/10.1056/NEJMc1814955) :heart:

### Tema 10: Medicina de Precisión II

 * Mojica, F. J. M., et al. (1995). [Long stretches of short tandem repeats are present in the largest replicons of the Archaea Haloferax mediterranei and Haloferax volcanii and could be involved in replicon partitioning.](https://www.ncbi.nlm.nih.gov/pubmed/7476211) :rocket:
 * la Chapelle, de, A. (2004). [Genetic predisposition to colorectal cancer.](https://www.ncbi.nlm.nih.gov/pubmed/15510158) :+1:
 * Mitelman, et al (2007). [The impact of translocations and gene fusions on cancer causation.](https://www.ncbi.nlm.nih.gov/pubmed/17361217) :exclamation:
 * Barrangou, R. et al. (2007). [CRISPR provides acquired resistance against viruses in prokaryotes.](https://www.ncbi.nlm.nih.gov/pubmed/17379808) :bomb:
 * Wiedenheft, B. et al. (2009). [Structural Basis for DNase Activity of a Conserved Protein Implicated in CRISPR-Mediated Genome Defense.](https://www.ncbi.nlm.nih.gov/pubmed/19523907) :exclamation:
 * Stratton, M. R., et al., (2009). [The cancer genome.](https://www.ncbi.nlm.nih.gov/pubmed/19360079) :+1:
 * Meyerson et al.,  (2010). [Advances in understanding cancer genomes through second-generation sequencing.](https://www.ncbi.nlm.nih.gov/pubmed/20847746) :rocket:
 * Hanahan, D., & R. A. Weinberg (2011). [Hallmarks of Cancer: The Next Generation.](https://www.ncbi.nlm.nih.gov/pubmed/21376230) :v:
 * Deltcheva, E., et al. (2011). [CRISPR RNA maturation by trans-encoded small RNA and host factor RNase III.](https://www.ncbi.nlm.nih.gov/pubmed/21455174) :exclamation:
 * Jinek, M., et al. (2012). [A programmable dual-RNA-guided DNA endonuclease in adaptive bacterial immunity.](https://www.ncbi.nlm.nih.gov/pubmed/22745249) :v: :bomb:
 * Greaves, M., & C. Maley (2012). [Clonal evolution in cancer.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3367003/) :+1:
 * Cong, L., et al. (2013). [Multiplex genome engineering using CRISPR/Cas systems.](https://www.ncbi.nlm.nih.gov/pubmed/23287718) :bomb:
 * Hoadley, K.A., et al. (2014). [Multiplatform analysis of 12 cancer types reveals molecular classification within and across tissues of origin.](https://www.ncbi.nlm.nih.gov/pubmed/25109877) :rocket:
 * Doudna, J. A., &  E. Charpentier (2014). [The new frontier of genome engineering with CRISPR-Cas9.](http://science.sciencemag.org/content/346/6213/1258096) :heart:
 * Baltimore, D. et al. (2015). [Biotechnology. A prudent path forward for genomic engineering and germline gene modification.](https://www.ncbi.nlm.nih.gov/pubmed/?term=25791083) :v:
 * Mertens, F., et al (2015). [The emerging complexity of gene fusions in cancer.](https://www.ncbi.nlm.nih.gov/pubmed/25998716) :exclamation:
 * Wan, J. C. M., et al. (2017). [Liquid biopsies come of age: towards implementation of circulating tumour DNA.](https://www.ncbi.nlm.nih.gov/pubmed/28233803) :heart:
 * Hellmann M., et al. (2018) [Tumor Mutational Burden and Efficacy of Nivolumab Monotherapy and in Combination with Ipilimumab in Small-Cell Lung Cancer.](https://www.ncbi.nlm.nih.gov/pubmed/29731394) :rocket:
 * Sanchez-Vega, et al. (2018). [Oncogenic Signaling Pathways in The Cancer Genome Atlas.](https://www.ncbi.nlm.nih.gov/pubmed/21376230). :v:


